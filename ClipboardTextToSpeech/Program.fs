open System.Runtime.InteropServices
open TextCopy
open System.Speech.Synthesis

let rec busyWait (synthesizer: string -> Async<unit>) current = async {
    let! _ = Async.Sleep 250
    let! res = ClipboardService.GetTextAsync() |> Async.AwaitTask
    let! _ =
        if not (res = current) then
            synthesizer res
        else
            async.Return ()
    return! busyWait synthesizer res
}


let windowsSpeakFactory speed =
    let synthesizer = new SpeechSynthesizer()
    synthesizer.Volume <- 100;  // 0...100
    synthesizer.Rate <- speed;     // -10...10
    fun (text : string) -> async {
       synthesizer.Speak(text)
       return ()
    }
        
let macSpeak (text: string) = async {
    use _ = System.Diagnostics.Process.Start("say",sprintf "\"%s\"" text)
    return ()
    }
    
[<EntryPoint>]
let main args =
    let r = ClipboardService.GetText()
    let speed =
        args
        |> Array.tryItem 0
        |> Option.defaultValue "2"
        |> int
    
    let synthFunc =
        if RuntimeInformation.IsOSPlatform(OSPlatform.Windows) then
            windowsSpeakFactory speed
        else
            macSpeak
    busyWait synthFunc r |> Async.RunSynchronously
    0 // return an integer exit code